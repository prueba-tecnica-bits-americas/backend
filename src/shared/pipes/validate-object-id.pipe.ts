import {PipeTransform, Injectable, ArgumentMetadata, BadRequestException} from '@nestjs/common'



@Injectable()
export class ValidateObjectId implements PipeTransform<any> {
    async transform(value: any, metada: ArgumentMetadata){
        if (isNaN(value)) {
            console.log(value, 'no es un numero')
            throw new BadRequestException('Invalid ID!')
        }

        return value;
    }
}