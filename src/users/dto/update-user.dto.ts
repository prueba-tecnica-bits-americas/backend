import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';
export class UpdateUserDto extends PartialType(CreateUserDto) {
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ required: true, minimum: 1 })
    readonly name: string;
  
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({ required: true, minimum: 1 })
    readonly email: string;
  
    @IsNumber()
    @IsNotEmpty()
    @ApiProperty({ required: true, minimum: 1 })
    readonly age: number;
  
    @IsString()
    @IsNotEmpty()
    @ApiProperty({ required: true, minimum: 1 })
    readonly gender: string;
}
