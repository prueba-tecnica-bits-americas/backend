import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'class-validator';
export class CreateUserDto {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ required: true, minimum: 1 })
  readonly id: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true, minimum: 1 })
  readonly name: string;

  @IsString()
  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({ required: true, minimum: 1 })
  readonly email: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty({ required: true, minimum: 1 })
  readonly age: number;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ required: true, minimum: 1 })
  readonly gender: string;
}
