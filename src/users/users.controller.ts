import { ValidateObjectId } from './../shared/pipes/validate-object-id.pipe';
import { User } from 'src/schemas/user.schema';
import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Delete,
  Put,
  HttpException,
  HttpStatus,
  UsePipes,
  ValidationPipe,
  Res,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import awaitToJs from 'await-to-js';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Post()
  @UsePipes(new ValidationPipe())
  async create(@Res() res, @Body() createUserDto: CreateUserDto) {
    const [err, userCreated] = await awaitToJs(this.usersService.create(createUserDto));
    if (err) {
      return new HttpException(err.message || err, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return res.status(HttpStatus.CREATED).json({
      message: 'User has been created Successfully',
      user: userCreated
    })
  }

  @Get()
  async findAll(@Res() res) {
    const [err, allUsers] = await awaitToJs(this.usersService.findAll());
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: err.message || err });
    }

    return res.status(HttpStatus.OK).json({
      message: 'These are all users',
      users: allUsers
    });
  }

  @Get(':id')
  async findOne(@Res() res, @Param('id', new ValidateObjectId()) id: number) {
    const [err, user] = await awaitToJs(this.usersService.findOne(+id));
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: err.message || err });
    }

    if (!user) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'User does Not Exist',
        user: null
      });
    }

    return res.status(HttpStatus.OK).json({
      message: 'User found Successfully',
      user
    });
  }

  @Put(':id')
  @UsePipes(new ValidationPipe())
  async update(@Res() res, @Param('id', new ValidateObjectId()) id: number, @Body() updateUserDto: UpdateUserDto) {
    const [err, userUpdated] = await awaitToJs(this.usersService.update(+id, updateUserDto));
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: err.message || err });
    }

    if (userUpdated.n && userUpdated.ok) {
      return res.status(HttpStatus.OK).json({
        message: 'User Updated Successfully',
        userUpdated
      });
    }

    if (!userUpdated.nModified) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'User does Not Exist',
        user: null
      });
    }

    return res.status(HttpStatus.OK).json({
      message: 'User Updated Successfully',
      userUpdated
    });
  }

  @Delete(':id')
  async remove(@Res() res, @Param('id', new ValidateObjectId()) id: number) {
    const [err, userDeleted] = await awaitToJs(this.usersService.remove(+id));
    if (err) {
      return res.status(HttpStatus.INTERNAL_SERVER_ERROR).json({ message: err.message || err });
    }

    if (!userDeleted.deletedCount) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'User does Not Exist',
        user: null
      });
    }

    return res.status(HttpStatus.OK).json({
      message: 'User deleted Successfully',
      userDeleted
    });
  }
}
